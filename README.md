# An example of Antora documentation site with openapi <!-- omit in toc -->

![Preview](preview.png)

- [Test it](#test-it)
- [How does it work](#how-does-it-work)

## Test it

```sh
docker run --rm -it -p 8080:8080 graham2071/antora-openapi-example
```

Open <http://localhost:8080>.

## How does it work

1. the openapi files are placed in [attachements](docs/modules/ROOT/attachments/).
2. for each openapi file, an [asciidoc template](docs/modules/ROOT/pages/petstore.adoc) is created. There is a little hack on the YOffset scroll to avoid some scroll issue.
3. the openapi files are rendered with [ReDoc](https://github.com/Redocly/redoc). There is no yaml to html generation.
4. some css style corrects a width issue on the page.
